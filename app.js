#!/usr/bin/env node
var Botkit = require('botkit');
var os = require('os');
var sheets = require('./sheets.js');
var feed = require('feed-read');

//將.env中的設定值，寫到 process.env 之中 
require('dotenv').config();

//新增 slack 耳朵來聽訊息
var slackEars = Botkit.slackbot({
    debug: true,
    //將資料存在 json 檔之中
    json_file_store: 'slackDataStore',
});
var pull = {};

//開始接上 slack RTM (Real Time Messaging)
var slackBot = slackEars.spawn({
    token: process.env.SLACK_BOT_TOKEN
}).startRTM();

//聽到 hello 
// slackEars.hears(['hello', 'hi', '哈囉'], 'direct_message,direct_mention,mention', function(bot, message) {

//     slackEars.storage.users.get(message.user, function(err, user) {
//         if (user && user.name) {
//             bot.reply(message, 'Hello ' + user.name + '!!');
//         } else {
//             bot.reply(message, 'Hello.');
//         }
//     });
//     // //回覆給使用者
//     // var r = Math.round(Math.random() * 10);
//     // // bot.reply(message, r.toString());
//     // switch(true){
//     //     case ( r == 1):
//     //         bot.whisper(message, {as_user: false, text: '最～喜～歡～你～了:heart:'});
//     //     case ( r <= 8 ):
//     //         bot.reply(message, "哈囉");
//     //         break;
//     //     default:
//     //         bot.reply(message, "...");
//     //         break;
//     // }
//     // // bot.reply(message, "");
// });

slackEars.hears(['megumi', 'めぐみ','恵'], 'direct_message,direct_mention', function(bot, message) {
    //回覆給使用者
    bot.reply(message, "あー、うん、そうだねー");
    // bot.reply(message, "");
});

slackEars.hears(['HM', '暉旻'], 'direct_message,direct_mention', function(bot, message) {
    //回覆給使用者
    bot.reply(message, "HM大大好強~~已跪");
    // bot.reply(message, "");
});

slackEars.hears(['prj (.*)', 'project (.*)', '專案 (.*)'], 'direct_message,direct_mention', function(bot, message){
    // console.log(message.match[1]);

    // bot.reply(message, function(){});
    var project_title = message.match[1];

    bot.startConversation(message,function(err,convo) {
        findProjects(project_title, convo);
        convo.next();
  });
});

slackEars.hears(['抽'], 'direct_message,direct_mention,mention', function(bot, message) {

    if(typeof pull[message.user] === 'number'){
        pull[message.user] += 1;
    }else{
        pull[message.user] = 0;
    }

    var sources = [
        'https://www.pinterest.com/meyjuliez/sexy.rss/',
        // 'http://funkang.blogspot.com/feeds/posts/default',
        'http://sexy-asian.tumblr.com/rss'
    ];
    var idx = 0;

    if(pull[message.user] == 3){
        bot.reply(message, '小心囉...');
    }
    if(pull[message.user] >= 3){
        idx = 1;
    }

    if(pull[message.user] < 7){
        loadFeed(sources[idx], function(res){
            bot.reply(message,{
                attachments: [
                    {
                        'text': res.name,
                        'image_url': res.img,
                    }
                ]
            });
        });
    }else{
        bot.reply(message, {
            attachments: [
                {
                    'text': '你這個壞孩子，讓姊姊懲罰你:heart:',
                    'image_url': 'https://pbs.twimg.com/media/CWxik1oU4AA4Ihp.jpg',
                }
            ]
        });

    }
});

//聽到 專案, project
slackEars.hears(['專案', 'project', 'prj'], 'direct_message,direct_mention', function(bot, message) {

    // start a conversation to handle this response.
    bot.startConversation(message, function(err, convo) {
        
        convo.ask('您要找什麼專案呢？交給我吧！\r\n(請輸入專案關鍵字)', [
            {
                pattern: 'no',
                callback: function(response, convo) {
                    convo.say('有需要隨時再呼叫我呦～');
                    convo.stop();
                }
            },
            {
                default: true,
                callback: function(response, convo){
                    findProjects(response.text, convo);
                    convo.next();
                }
            },
        ]);
    })
});



function _find(key, convo, callback){
    sheets.data(function(res){
        var data = res.values;
        var keys = data[0];
        var res = [];
        
        var index = {
            server_type     : keys.indexOf('主機類型'),
            project_type    : keys.indexOf('類型'),
            nicetitle       : keys.indexOf('暱稱'),
            title           : keys.indexOf('網站名稱'),
            url             : keys.indexOf('網站'),
            designer        : keys.indexOf('設計師'),
            engineer        : keys.indexOf('工程師'),
            status          : keys.indexOf('狀態'),
            server_service  : keys.indexOf('主機商'),
            ip_addr         : keys.indexOf('IP'),
            dns_service     : keys.indexOf('網域商'),
            dns_is_customer : keys.indexOf('客戶網域'),
            cdn_service     : keys.indexOf('NS'),
            ssl_certificate : keys.indexOf('SSL憑證'),
            trello          : keys.indexOf('Trello'),
            google_drive    : keys.indexOf('Google Drive'),
            version         : keys.indexOf('壓版版本'),
            archived_at     : keys.indexOf('封存日期'),
        };
        convo.say("從 "+data.length+" 筆資料中尋找符合條件的專案..");
        for(var i=1;i<data.length;i++){

            var hasData = false;
            if(typeof data[i][index.nicetitle] !== 'undefined'){
                switch(true){
                    case data[i][index.nicetitle].indexOf(key)  != -1:
                    case data[i][index.title].indexOf(key)      != -1:
                        hasData = true;
                        break;
                    case data[i][index.url].indexOf(key) != -1:
                        hasData = true;
                        break;
                }
            }
            if(hasData){
                var tmp = {};
                Object.keys(index).map(function(key, ii) {
                    tmp[key] = data[i][index[key]];
                });
                res.push(tmp);
            }
        }
        callback(res);
    });
}

function findProjects(project_title, convo) {
    convo.say('「' + project_title + '」是嗎？讓我找找看喔..');
    _find(project_title, convo, function(res){
        // convo.say('test 1');
        if(res.length <= 0){
            // convo.say('test 2-1');
            convo.say({
                attachments:[
                {
                    'title':'嗚嗚，找不到那個專案耶 >.<;',
                    'text':'還沒新增到網站列表嗎？點擊連結新增吧！\r\n',
                    "actions": [
                        {
                            "type": "button",
                            "text": "新增資料",
                            "url": "https://docs.google.com/spreadsheets/d/17e1Wpyn459h_8Ps5UcjgeoyrFlBKNgw8wxZJ_jiIAw0/edit#gid=0",
                            "style": 'danger'
                        }
                    ],
                }]
            });
        }else{
            // convo.say('test 2-2');
            if(res.length > 5){
                // convo.say('test 3-1');
                convo.say('符合條件的專案太多了，請輸入更明確的條件喔 >.<');
            }else{
                // convo.say('test 3-2');
                convo.say('一共找到 ' +res.length+ ' 個專案喔：');
                res.forEach(function(r){
                    var status = r.status;
                    if(r.status == '停止維護'){
                        status += ' - ' + r.archived_at;
                    }
                    convo.say({ 
                        attachments: [
                        {
                            'title': r.title + ' ' + r.url,
                            'text': r.server_type + ' - ' + r.ip_addr + ' [' + status +']',
                            'color': '#ff2d8e',
                            "author_name": 'By ' + r.designer + ' & ' + r.engineer,
                            "title_link": r.url,
                            "actions": [
                                {
                                    "type": "button",
                                    "text": "Trello",
                                    "url": r.trello,
                                    // "style": 'primary'
                                },
                                {
                                    "type": "button",
                                    "text": "Google Drive",
                                    "url": r.google_drive,
                                    // "style": 'default'
                                },
                            ],
                            "footer": r.project_type + ' ' + r.version,
                        }
                        ]
                    });
                });
            }
        }
    });
}

function loadFeed(url, callback){
    feed(url, function(err, articles) {
        if (err) throw err;
        var rand = Math.round(Math.random() * articles.length);

        var src = null;

        while(src == null){
            rand = (Math.round(Math.random() * articles.length)) % articles.length;
            src = articles[rand]['content'].match('src\s*=\s*"(.+?)"');
        }
        // console.log(src[1]);
        callback({
            name: articles[rand]['title'],
            img: src[1]
        });
    });
}